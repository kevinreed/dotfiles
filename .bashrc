set -o vi

case `uname` in
Darwin)
  alias l="ls -FG"
  alias la="ls -FGA"
  alias ll="ls -FGlh"
  alias lla="ls -FGAlh"
  alias d="df -T nodevfs,autofs -H"
  export GREP_COLOR='38;5;159;1' # MacOS has old grep
  unset LC_CTYPE
  ;;
Linux)
  alias l='ls --time-style=long-iso --color=auto -Fh --group-directories-first'
  alias la='ls -A --time-style=long-iso --color=auto -Fh --group-directories-first'
  alias ll='ls -l --time-style=long-iso --color=auto -Fh --group-directories-first'
  alias lla='ls -lA --time-style=long-iso --color=auto -Fh --group-directories-first'
  alias d="df -l -x tmpfs -x devtmpfs -H"
  ;;
*)
  echo "IDK, what is this"
  ;;
esac

alias m="less"
alias g="grep -i --color=auto"
alias ff="find . -type f -ls"
alias speed='curl -L --silent -o /dev/null -w "%{url_effective} (%{remote_ip}): %{http_code}/%{num_redirects} , Connect: %{time_connect}s, Lookup: %{time_namelookup}s, Total: %{time_total}s\n"'

function p() {
  if [ -z $1 ]
  then
  {
    ps --pid 2 --ppid 2 --deselect -H -o pid,user,state,pcpu,vsize:8,args
  }
  else
  {
    ps -o pid,user,state,pcpu,vsize:8,args -p `pgrep $1`
  }
  fi
}

if [ -f /opt/local/etc/profile.d/bash_completion.sh ]; then
  . /opt/local/etc/profile.d/bash_completion.sh
fi


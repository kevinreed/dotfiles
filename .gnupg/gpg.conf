# No copyright notice
no-greeting

# Native charset
charset utf-8

# do not put version and comment into PGP MESSAGE block
no-emit-version
no-comments

# Long key id
keyid-format long

# see if the keys are valid and display fingerprint
with-fingerprint
list-options show-uid-validity
verify-options show-uid-validity show-policy-urls show-notations show-keyserver-urls

# Keyservers for searching and uploading keys, use pre-defined certificate for sks-keyservers.net
#keyserver hkp://keys.gnupg.net
keyserver hkps://hkps.pool.sks-keyservers.net

# always use configured server and not the one supplied with a key
keyserver-options no-honor-keyserver-url

# Keyserver options: auto retrieve keys when searching or discovering a revoked key
#keyserver-options auto-key-retrieve

# Crypto preferences
personal-cipher-preferences AES256 AES
personal-digest-preferences SHA512 SHA256
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed

# Key validation preferences
ask-cert-level
default-cert-level 2
cert-digest-algo SHA512

# Secret key encryption preferences
s2k-cipher-algo AES256
s2k-digest-algo SHA512
s2k-mode 3
s2k-count 65000000

# export only minimal key
# the rest should be fetched from a keyserver
export-options export-minimal

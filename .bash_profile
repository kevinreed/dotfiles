export LANG="en_US.UTF-8"
export PS1="\[\e[38;5;220m\]\u@\h \W\[\e[0;1m\]\$\[\e[0m\] "
export PATH="${HOME}/bin:/opt/local/bin:/opt/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export LESS="-FgMqRX"

if [ -r ${HOME}/.bashrc ] ; then
  . ${HOME}/.bashrc
fi

for i in /etc/profile.d/*.sh ; do
  if [ -r "$i" ]; then
    . "$i"
  fi
done


if [ ! -z "${SSH_CLIENT}" ]; then
  if [ -x /usr/bin/screen ]; then
    screen -R
  fi
fi

export PATH=${HOME}/bin:${PATH}:/opt/local/bin:/opt/local/sbin:/usr/local/go/bin
export GOPATH=${HOME}/Documents/go
export PAGER=less
export LANG=en_US.UTF-8

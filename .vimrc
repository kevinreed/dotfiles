" Explicitly define script encoding to be compatible with Windows and non-UTF locales
scriptencoding utf-8

set nocompatible    " enable vim features
set autoindent      " copy indent from whe current line when opening a new line
set nobackup        " wherever I need a backup, I use version control
set backspace=2     " backspace:  '2' allows backspacing" over indentation, end-of-line and start-of-line
set noerrorbells    " Keep vim quiet
set showmatch       " Show the matching bracket for the last ')'? 
set shortmess=atI   " Reduce the verbosity of the most common messages and remove :intro
set visualbell      " Keep vim quiet
set background=dark " I'm running it on black-backgrounded xterm
set tildeop         " make a tilde ("~") an operator, to use it like "~W"

" Make Y yank everything from the cursor to the end of the line. This makes Y to act more like C or D because by default, Y yanks 
" the current line (i.e. the same as yy).
noremap Y y$

if has('autocmd')
  filetype plugin indent on
endif

" hlsearch :  highlight search - show the current search pattern
" but have an ability to remove highliting when done
set hlsearch
nnoremap <silent> <leader>/ :nohls<enter>

" Keep tabstop right for compatibility, but 
" use softtabstop to have 2 spaces per <Tab> hit
" Use expandtab to avoid mixing tabs and spaces which breaks Python code
set tabstop=8
set softtabstop=2
set expandtab
set smarttab
" Keep normal tabs for some filetypes where it's needed
au FileType crontab,fstab,make setlocal noet ts=8 sw=8

" shiftwidth:  Number of spaces to use for each insertion of (auto)indent.
" match is to softtabstop
" shiftround to line up when indenting
set shiftwidth=2
set shiftround

if has("syntax")
  syntax on
endif

" wrapmargin allows to wrap text automagically
" but for some types set textwidth explicitly
set wrapmargin=5
autocmd FileType html setlocal textwidth=132
autocmd FileType c,python setlocal textwidth=132 shiftwidth=2 softtabstop=2 cursorline cindent cinkeys-=0#
autocmd FileType asciidoc,markdown setlocal textwidth=132 spell shiftwidth=2 softtabstop=2
autocmd FileType yaml setlocal cursorline


" use modeline
set modeline
set modelines=3

if !has('gui_running')
  set t_Co=256
endif

" GUI option
set guifont=Cascadia_Mono:h14:cANSI:qDRAFT
set guioptions-=T
set guioptions-=m
colorscheme hybrid
highlight Comment ctermfg=243 guifg=#b0b0b0

" show whitespaces at the end of line
" switch it on and off with a <leader>l
if has('multi_byte') || has('listchars')
  set listchars=tab:»\ ,trail:·
  nnoremap <silent> <leader>l :setlocal list!<CR>
endif
nnoremap <silent> <leader>p :setlocal paste!<CR>

function! Numbers()
" cycle between 3 settings
" 0. set nonumner, set norelativenumber
" 1. set number, set norelativenumber
" 2. set number, set relativenumber
  if  &l:number == 1 && &l:relativenumber == 0
    setlocal number
    setlocal relativenumber
  elseif &l:number == 1 && &l:relativenumber == 1
    setlocal nonumber
    setlocal norelativenumber
  else
    setlocal number
    setlocal norelativenumber
  endif
endfunction
nnoremap <silent> <leader>n :call Numbers()<CR>
highlight CursorLineNr term=bold cterm=bold ctermfg=249 gui=bold guifg=Yellow
highlight LineNr cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

" Use Q to replay q buffer, which I use most frequently
nnoremap <silent> Q @q
vnoremap <silent> Q :norm @q<cr>

" statusline
set laststatus=2  " Always show the status line
set noshowmode    " Not needed with status line highlights
highlight statusline cterm=NONE ctermfg=254 ctermbg=67 gui=NONE guifg=#e4e4e4 guibg=#5f87af
highlight statuslineNC cterm=NONE ctermfg=007 ctermbg=008 gui=NONE
highlight User1 cterm=NONE ctermfg=195 ctermbg=67 gui=NONE guifg=#d7ffff guibg=#5f87af
set statusline=\ %.20f    " Path to the file
set statusline+=%=        " Switch to the right side
set statusline+=%{&paste==1?'[PASTE]\ ':''} " Paste mode indicator
set statusline+=%c	  " Current column
set statusline+=%1*:%0*	  " Separator
set statusline+=%l        " Current line
set statusline+=%1*/%0*   " Separator
set statusline+=%L        " Total lines
set statusline+=\         " Padding

function! ChangeStatusColor(mode)
  if a:mode == 'i'
    highlight statusline cterm=NONE ctermfg=254 ctermbg=036 gui=NONE guifg=#e4e4e4 guibg=#00af87
    highlight User1 cterm=NONE ctermfg=057 ctermbg=036 gui=NONE guifg=#5f00ff guibg=#00af87
  elseif a:mode == 'r'
    highlight statusline cterm=NONE ctermfg=254 ctermbg=202 gui=NONE guifg=#e4e4e4 guibg=#ff5f00
    highlight User1 cterm=NONE ctermfg=075 ctermbg=202 gui=NONE guifg=#5fafff guibg=#ff5f00
  else
    highlight statusline cterm=NONE ctermfg=254 ctermbg=67 gui=NONE guifg=#e4e4e4 guibg=#5f87af
    highlight User1 cterm=NONE ctermfg=195 ctermbg=67 gui=NONE guifg=#d7ffff guibg=#5f87af
  endif
endfunction

au InsertEnter * call ChangeStatusColor(v:insertmode)
au InsertLeave * call ChangeStatusColor('n')

" Cursor line
nnoremap <silent> <leader>c :setlocal cursorline!<CR>
highlight CursorLine cterm=NONE ctermbg=235 ctermfg=NONE gui=NONE guifg=NONE guibg=#262626

" set lower timeout so that shift-O doesn't take as long
" see https://github.com/vim/vim/issues/24
set timeout timeoutlen=5000 ttimeoutlen=100

" Use buffers like tabs
set hidden                " Do not complain about switching from unsaved buffer
nnoremap <silent> <C-N> :bnext<cr>
nnoremap <silent> <C-P> :bprevious<cr>
nnoremap <silent> <leader>1 :1buffer<cr>
nnoremap <silent> <leader>2 :2buffer<cr>
nnoremap <silent> <leader>3 :3buffer<cr>
nnoremap <silent> <leader>4 :4buffer<cr>
nnoremap <silent> <leader>5 :5buffer<cr>
nnoremap <silent> <leader>6 :6buffer<cr>
nnoremap <silent> <leader>7 :7buffer<cr>
nnoremap <silent> <leader>8 :8buffer<cr>
nnoremap <silent> <leader>9 :9buffer<cr>
noremap <C-k> <C-w>k
noremap <C-j> <C-w>j

" buftabline
let g:buftabline_show = 1
hi BufTabLineCurrent term=bold,reverse ctermfg=15 ctermbg=63 guifg=#ffffff guibg=#5f5fff
hi BufTabLineActive ctermfg=117 ctermbg=63 guifg=#b0e7ff guibg=#5f5fff
hi BufTabLineHidden ctermfg=7 ctermbg=63 guifg=#c0c0c0 guibg=#5f5fff
hi BufTabLineFill ctermfg=254 ctermbg=63 guifg=#f0f0f0 guibg=#5f5fff

" bufexplorer
let g:bufExplorerDisableDefaultKeyMapping=1
let g:bufExplorerDefaultHelp=0
nnoremap <silent> <leader>b :ToggleBufExplorer<cr>

" Built-in plugin to improve % behaviour
silent! packadd! matchit

" when editing vim configs use K to call for :help
autocmd FileType vim setlocal keywordprg=:help textwidth=132
autocmd FileType xml setlocal equalprg=xmllint\ --format\ -
autocmd FileType xml setlocal equalprg=jq\ .

" create tags for source code
command! Tags !ctags -R .

" netrw
let g:netrw_banner = 0
nnoremap <silent> <leader>e :Explore<cr>

" disable arrows and force hjkl use for movement
map <Up> <nop>
map <Down> <nop>
map <Left> <nop>
map <Right> <nop>

" Make ^u and ^w to be a separate change in undo history
inoremap <C-U> <C-G>u<C-U>
inoremap <C-W> <C-G>u<C-W>

" vimwiki: use Markdown
let g:vimwiki_list = [{'path': '~/.notes/', 'ext': '.md', 'syntax': 'markdown'}]
